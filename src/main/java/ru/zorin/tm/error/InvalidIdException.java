package ru.zorin.tm.error;

public class InvalidIdException extends RuntimeException {

    public InvalidIdException(Throwable cause) {
        super(cause);
    }

    public InvalidIdException() {
        super("Invalid id");
    }
}
