package ru.zorin.tm.error;

public class InvalidIndexException extends RuntimeException{

    public InvalidIndexException(Throwable cause) {
        super(cause);
    }

    public InvalidIndexException() {
        super("Invalid index");
    }
}
