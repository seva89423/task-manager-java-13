package ru.zorin.tm.error;

public class InvalidNameException extends RuntimeException {

    public InvalidNameException(Throwable cause) {
        super(cause);
    }

    public InvalidNameException() {
        super("Invalid name");
    }
}
