package ru.zorin.tm.error;

public class ProjectEmptyException extends RuntimeException{

    public ProjectEmptyException(Throwable cause) {
        super(cause);
    }

    public ProjectEmptyException() {
        super("Project is empty");
    }
}
