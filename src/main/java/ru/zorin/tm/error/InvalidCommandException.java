package ru.zorin.tm.error;

public class InvalidCommandException extends RuntimeException {

    public InvalidCommandException(Throwable cause) {
        super(cause);
    }

    public InvalidCommandException() {
        super("Invalid command");
    }
}
