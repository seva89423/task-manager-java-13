package ru.zorin.tm.error;

public class TaskUpdateException extends RuntimeException {

    public TaskUpdateException(Throwable cause) {
        super(cause);
    }

    public TaskUpdateException() {
        super("Fail to update task");
    }
}
