package ru.zorin.tm.error;

public class TaskEmptyException extends RuntimeException {

    public TaskEmptyException(Throwable cause) {
        super(cause);
    }

    public TaskEmptyException() {
        super("Task is empty");
    }
}
