package ru.zorin.tm.error;

public class ProjectUpdateException extends RuntimeException{

    public ProjectUpdateException(Throwable cause) {
        super(cause);
    }

    public ProjectUpdateException() {
        super("Fail to update project");
    }
}
