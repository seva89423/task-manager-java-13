package ru.zorin.tm.service;

import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.api.service.IProjectService;
import ru.zorin.tm.error.InvalidIdException;
import ru.zorin.tm.error.InvalidIndexException;
import ru.zorin.tm.error.InvalidNameException;
import ru.zorin.tm.error.ProjectEmptyException;
import ru.zorin.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    final private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new ProjectEmptyException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new ProjectEmptyException();
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return projectRepository.findProjectById(id);
    }

    @Override
    public Project findProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new InvalidIndexException();
        return projectRepository.findProjectByIndex(index);
    }

    @Override
    public Project findProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        return projectRepository.getProjectByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Project project = findProjectById(id);
        if (project == null) throw new ProjectEmptyException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Project project = findProjectByName(name);
        if (project == null) throw new ProjectEmptyException();
        return projectRepository.removeProjectByName(name);
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        if (index == null || index < 0) throw new InvalidIndexException();
        final Project project = findProjectByIndex(index);
        if (project == null) throw new ProjectEmptyException();
        return projectRepository.removeProjectByIndex(index);
    }

    @Override
    public Project removeProjectById(final String id) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return projectRepository.removeProjectById(id);
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Project project = findProjectByIndex(index);
        if (project == null) throw new ProjectEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}