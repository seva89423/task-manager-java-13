package ru.zorin.tm.service;

import ru.zorin.tm.api.repository.ICommandRepository;
import ru.zorin.tm.api.service.ICommandService;
import ru.zorin.tm.model.Command;;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }
}
