package ru.zorin.tm.service;

import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.api.service.ITaskService;
import ru.zorin.tm.error.InvalidIdException;
import ru.zorin.tm.error.InvalidIndexException;
import ru.zorin.tm.error.InvalidNameException;
import ru.zorin.tm.error.TaskEmptyException;
import ru.zorin.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    final private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name){
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description){
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new TaskEmptyException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskEmptyException();
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new InvalidIndexException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        return taskRepository.getTaskByName(name);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task =findOneById(id);
        if (task == null) throw new TaskEmptyException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
       if (index == null || index < 0) throw new InvalidIndexException();
       return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskEmptyException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }
}