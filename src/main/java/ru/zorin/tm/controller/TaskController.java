package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.ITaskController;
import ru.zorin.tm.api.service.ITaskService;
import ru.zorin.tm.error.InvalidIndexException;
import ru.zorin.tm.error.TaskEmptyException;
import ru.zorin.tm.error.TaskUpdateException;
import ru.zorin.tm.model.Task;
import ru.zorin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    public ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) System.out.println(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[COMPLETE]");
    }

    private void showTask(Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[ERROR]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.findOneByIndex(index);
            if (task == null) throw new TaskEmptyException();
            showTask(task);
            System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) throw new TaskEmptyException();
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) throw new TaskEmptyException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) throw new TaskUpdateException();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.findOneByIndex(index);
            if (task == null) throw new TaskEmptyException();
            System.out.println("ENTER NAME:");
            final String name = TerminalUtil.nextLine();
            System.out.println("[ENTER DESCRIPTION]");
            final String description = TerminalUtil.nextLine();
            final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
            if (taskUpdated == null) throw new TaskUpdateException();
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) throw new TaskEmptyException();
        else System.out.println("[COMPLETE]");
    }


    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) throw new TaskEmptyException();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        try {
            final Integer index = TerminalUtil.nextNumber() - 1;
            final Task task = taskService.removeOneByIndex(index);
            if (task == null) throw new TaskEmptyException();
            if (index == null || index < 0) throw new InvalidIndexException();
            System.out.println("[COMPLETE]");
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidIndexException();
        }
    }
}