package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.ICommandController;
import ru.zorin.tm.api.service.ICommandService;
import ru.zorin.tm.model.Command;
import ru.zorin.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.HELP);
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.EXIT);
        System.out.println(Command.TASK_CREATE);
        System.out.println(Command.TASK_CLEAR);
        System.out.println(Command.TASK_LIST);
        System.out.println(Command.PROJECT_CREATE);
        System.out.println(Command.PROJECT_CLEAR);
        System.out.println(Command.PROJECT_LIST);
        System.out.println(Command.TASK_UPDATE_BY_INDEX);
        System.out.println(Command.TASK_UPDATE_BY_ID);
        System.out.println(Command.TASK_VIEW_BY_ID);
        System.out.println(Command.TASK_VIEW_BY_NAME);
        System.out.println(Command.TASK_VIEW_BY_INDEX);
        System.out.println(Command.TASK_REMOVE_BY_ID);
        System.out.println(Command.TASK_REMOVE_BY_NAME);
        System.out.println(Command.TASK_REMOVE_BY_INDEX);
        System.out.println(Command.PROJECT_UPDATE_BY_INDEX);
        System.out.println(Command.PROJECT_UPDATE_BY_ID);
        System.out.println(Command.PROJECT_VIEW_BY_ID);
        System.out.println(Command.PROJECT_VIEW_BY_NAME);
        System.out.println(Command.PROJECT_VIEW_BY_INDEX);
        System.out.println(Command.PROJECT_REMOVE_BY_ID);
        System.out.println(Command.PROJECT_REMOVE_BY_NAME);
        System.out.println(Command.PROJECT_REMOVE_BY_INDEX);
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.8");
    }

    public void showCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void showArguments() {
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }
}