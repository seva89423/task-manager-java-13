package ru.zorin.tm.bootstrap;

import ru.zorin.tm.api.controller.ICommandController;
import ru.zorin.tm.api.controller.IProjectController;
import ru.zorin.tm.api.controller.ITaskController;
import ru.zorin.tm.api.repository.ICommandRepository;
import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.api.service.ICommandService;
import ru.zorin.tm.api.service.IProjectService;
import ru.zorin.tm.api.service.ITaskService;
import ru.zorin.tm.constant.ArgumentConst;
import ru.zorin.tm.constant.TerminalConst;
import ru.zorin.tm.controller.CommandController;
import ru.zorin.tm.controller.ProjectController;
import ru.zorin.tm.controller.TaskController;
import ru.zorin.tm.error.InvalidCommandException;
import ru.zorin.tm.repository.CommandRepository;
import ru.zorin.tm.repository.ProjectRepository;
import ru.zorin.tm.repository.TaskRepository;
import ru.zorin.tm.service.CommandService;
import ru.zorin.tm.service.ProjectService;
import ru.zorin.tm.service.TaskService;
import ru.zorin.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        System.out.println("***WELCOME TO TASK MANAGER***");
        if (parseArgs(args)) System.exit(0);
        while (true){
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e){
                System.err.println("[ERROR]");
                System.err.println(e.getMessage());
            }
        }
    }
    
    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showInfo();
                break;
            default:
                throw new InvalidCommandException();
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                commandController.showHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                throw new InvalidCommandException();
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public static void exit() {
        System.exit(0);
    }
}