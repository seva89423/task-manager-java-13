package ru.zorin.tm.repository;

import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.error.TaskEmptyException;
import ru.zorin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task){
        tasks.add(task);
    }

    @Override
    public void remove(Task task){
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(){
        return tasks;
    }

    @Override
    public void clear(){
        tasks.clear();
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task getTaskByName(final String name) {
        for (final Task task:tasks){
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = getTaskByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskEmptyException();
        remove(task);
        return task;
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task:tasks){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) throw new TaskEmptyException();
        tasks.remove(task);
        return task;
    }
}
