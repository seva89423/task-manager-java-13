package ru.zorin.tm.repository;

import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.error.ProjectEmptyException;
import ru.zorin.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findProjectByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project getProjectByName(final String name) {
        for (final Project project:projects){
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeProjectByName(final String name) {
        final Project project = getProjectByName(name);
        if (project == null) throw new ProjectEmptyException();
        remove(project);
        return project;
    }

    @Override
    public Project removeProjectByIndex(final Integer index) {
        final Project project = findProjectByIndex(index);
        if (project == null) throw new ProjectEmptyException();
        remove(project);
        return project;
    }

    @Override
    public Project findProjectById(final String id) {
        for (final Project task:projects){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Project removeProjectById(final String id) {
        final Project project = findProjectById(id);
        if (project == null) throw new ProjectEmptyException();
        projects.remove(project);
        return project;
    }
}
