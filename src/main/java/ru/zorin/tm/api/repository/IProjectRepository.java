package ru.zorin.tm.api.repository;

import ru.zorin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findProjectById(String id);

    Project findProjectByIndex(Integer index);

    Project getProjectByName(String name);

    Project removeProjectByName(String name);

    Project removeProjectByIndex(Integer index);

    Project removeProjectById(String id);
}
