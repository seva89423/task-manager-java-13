package ru.zorin.tm.api.repository;

import ru.zorin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add (Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task getTaskByName(String name);

    Task removeOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneById(String id);
}
