package ru.zorin.tm.api.service;

import ru.zorin.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
